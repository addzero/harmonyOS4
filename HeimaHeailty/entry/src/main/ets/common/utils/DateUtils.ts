export default class DateUtils{
  static beginTimeOfDate(date:Date){
    // 获取日期对象的时间戳（包含时分秒）
    const timestampWithTime = date.getTime();

    // 创建一个新的Date对象，将时间设置为1970-01-01 00:00:00
    const dateWithoutTime = new Date(1970, 0, 1, 0, 0, 0, 0);

    // 将包含时分秒的时间戳赋值给不含时分秒的日期对象
    dateWithoutTime.setTime(timestampWithTime);

    // 返回不包含时分秒的时间戳
    return dateWithoutTime.getTime();
  }

  static formatDateTime(dateTime:number){
    let date = new Date(dateTime)
    // 获取年、月、日
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // 月份是从0开始的，所以需要+1
    const day = date.getDate();

    // 格式化月和日，如果不足两位数，前面补0
    const formattedMonth = month < 10 ? '0' + month : month;
    const formattedDay = day < 10 ? '0' + day : day;

    // 返回格式化的日期字符串
    return `${year}/${formattedMonth}/${formattedDay}`;
  }
}