export default class RouterItem {
  url: string
  title: string

  constructor(url, title) {
    this.url = url
    this.title = title
  }
}