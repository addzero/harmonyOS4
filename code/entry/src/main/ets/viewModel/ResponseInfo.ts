class responseData{
  total: number
  totalPages: number
  rows: any[]
}

export default class ResponseInfo{
  code: number
  data: responseData
}