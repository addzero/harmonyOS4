import { getShopModelListFun } from '../api/ShopModelApi'

class ShopModel {
  pageNo: number = 1
  pageSize: number = 3

  getListFun() {
    return getShopModelListFun(this.pageNo,this.pageSize)
  }
}

export default new ShopModel()