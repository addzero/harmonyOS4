let express = require('express');
let app = express();
let allData = require("./data.json")
app.use('/images', express.static('images')); // 设置静态资源目录

app.get("/shop", (req, res) => {
    console.log(req.query,'接收的参数')
    let {pageNo, pageSize} = req.query
    // 确保pageNo和pageSize是正整数
    pageNo = Math.max(1, parseInt(pageNo, 10));
    pageSize = Math.max(1, parseInt(pageSize, 10));
    // 计算起始索引和结束索引
    let startIndex = (pageNo - 1) * pageSize;
    let endIndex = startIndex + pageSize;
    // 返回当前页的数据
    let currentPageData = allData.slice(startIndex, endIndex);
    // 返回总页数
    let totalPages = Math.ceil(allData.length / pageSize);
    res.send({
        code: "200",
        data: {
            total: allData.length,
            rows: currentPageData,
            totalPages: totalPages
        }
    })
})

app.listen(3000, () => {
    console.log(`服务启动成功 http://localhost:3000`)
})