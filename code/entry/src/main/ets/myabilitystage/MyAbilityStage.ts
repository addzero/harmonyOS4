import AbilityStage from '@ohos.app.ability.AbilityStage';
import Want from '@ohos.app.ability.Want';
export default class MyAbility extends AbilityStage{
  onAcceptWant(want:Want): string{
    // 判断被启动的Ability的名称
    if(want.abilityName === "DocumentAbility"){
      return `DocumentAbility_${want.parameters.instanceKey}`
    }
    return ""
  }
}