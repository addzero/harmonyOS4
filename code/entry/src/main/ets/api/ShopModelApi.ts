import service from "../utils/service"

/**
 * 获取商铺列表方法
 * @param pageNo
 * @param pageSize
 * @returns
 */
export function getShopModelListFun(pageNo, pageSize) {
  return service({
    url: "/shop",
    method: "get",
    params: {
      pageNo,
      pageSize
    }
  })
}